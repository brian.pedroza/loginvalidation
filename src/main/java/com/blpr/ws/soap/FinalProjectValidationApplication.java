package com.blpr.ws.soap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinalProjectValidationApplication {

	public static void main(String[] args) {
		SpringApplication.run(FinalProjectValidationApplication.class, args);
	}

}
