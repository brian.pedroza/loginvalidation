package com.blpr.ws.soap;

import org.apache.cxf.feature.Features;

import com.blpr.ws.trainings.GetUserRequest;
import com.blpr.ws.trainings.GetUserResponse;
import com.blpr.ws.trainings.User;
import com.blpr.ws.trainings.UserPortType;

@Features(features="org.apache.cxf.feature.LoggingFeature")
public class UserValidationImpl implements UserPortType{
	
	User user = new User();
	
	
	public UserValidationImpl() {
		init();
	}
	public void init() {
		user.setName("brian");
		user.setPassword("1234");
		
	}
	

	@Override
	public GetUserResponse getUser(GetUserRequest request) {
		String name = request.getName();
		String password = request.getPassword();
		GetUserResponse response = new GetUserResponse();
		System.out.println("name: " + name + " " + user.getName() + "password" + password + " " + user.getPassword());
		if(name.equals(user.getName()) && password.equals(user.getPassword())) {
			response.setResult(true);
		} 
		else response.setResult(false);
		return response;
		
	}
}
